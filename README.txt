# FlexNav jQuery Plugin - Drupal module

This module provides integration with the FlexNav jQuery plugin for a
responsive menu. A single responsive menu block is provided for the main menu.


* * *
### INSTALLATION

1) Download and install the jQuery Update module as usual (see below)
   http://drupal.org/project/jquery_update

2) Update your jQuery version to at least 1.7 via the jQuery Update module
   settings page located at admin/config/development/jquery_update.

3) Install the FlexNav jQuery plugin in one of two ways:

  Using Drush Make
  ----------------

  a) Add the FlexNav project to a drush make file and the library will
     automatically be downloaded into the sites/all/libraries folder.

  Manually Install
  ----------------

  a) Download the FlexNav jQuery plugin from github:
     http://github.com/indyplanets/flexnav

  b) Unpack the FlexNav jQuery plugin, and place it in your sites/all/libraries
     directory (or sites/specific-site.com/libraries directory for multi-site)

  c) Rename the 'flexnav-master' directory to simply 'flexnav'

4) Install the FlexNav module as usual (see below)
   http://drupal.org/project/flexnav


* * *
### USAGE

1) Navigate to the blocks administration at admin/structure/blocks

2) Place the Responsive Main Menu (FlexNav) block into a region

3) Profit


* * *
### STANDARD MODULE INSTALLATION INSTRUCTIONS

* Some documentation on standard module installation for Drupal 7:
  http://drupal.org/documentation/install/modules-themes/modules-7


* * *
### REQUIREMENTS

* jQuery Update module (http://drupal.org/project/jquery_update)
* Libraries API module (http://drupal.org/project/libraries)
* FlexNav jQuery Plugin (http://jasonweaver.name/lab/flexiblenavigation)


* * *
### MAIN CONTRIBUTORS

* jQuery Plugin created by Jason Weaver (http://github.com/indyplanets)
* Drupal 7 module created by Jen Lampton (http://drupal.org/user/85586)
* Drupal 7 module also maintained by Jon Antoine (https://drupal.org/user/192192)
